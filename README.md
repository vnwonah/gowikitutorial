# README #

Golang Wiki Tutorial

### How do I get set up? ###

clone repo and run `./wiki`

or edit source then `go build wiki.go`
then run `./wiki`

To create a new  page append /edit/`newpage` to `localhost:4000`

to password a page simply fill the password box and save after editing page
new passwords overwrrite old ones

to allow a page for public  edit leave password box blank.
